# Custom Built Software Repository

This is a repository of software packages that I have compiled to my own specifications. All of the packages are open source and freely available, but I have compiled them with specific options and configurations that may not be available in the standard distribution.

## Package List

- `aria2`: a lightweight multi-protocol & multi-source download utility.
- `curl`: a command-line tool for transferring data specified with URL syntax.
- `gcc`: the GNU Compiler Collection, a compiler system that supports various programming languages.
- `sqlite`: a C-language library that implements a small, fast, self-contained, high-reliability, full-featured, SQL database engine.
- `wget`: a free utility for non-interactive download of files from the Web.
- `wget2`: the successor to the original `wget` utility, with improved features and performance.

## Package Format

All of the packages are compressed using the `tar.xz` format. To make them easier to share and distribute, I have also encoded them as base64 text files. To use the packages, simply decode them using the `base64` command and then extract the contents using the `tar` command.

## Downloading, decoding and unpacking

```bash
curl -fsSL https://gitlab.com/hecdelatorre/custom-built-software/-/raw/main/x64/aria2-1.36.0.txz.txt | base64 -d | tar Jxvf -
```

```bash
curl -fsSL https://gitlab.com/hecdelatorre/custom-built-software/-/raw/main/x64/curl-8.0.1.txz.txt | base64 -d | tar Jxvf -
```

```bash
curl -fsSL https://gitlab.com/hecdelatorre/custom-built-software/-/raw/main/x64/gcc-11.3.0.txz.txt | base64 -d | tar Jxvf -
```

```bash
curl -fsSL https://gitlab.com/hecdelatorre/custom-built-software/-/raw/main/x64/sqlite-3.41.2.txz.txt | base64 -d | tar Jxvf -
```

```bash
curl -fsSL https://gitlab.com/hecdelatorre/custom-built-software/-/raw/main/x64/wget-1.21.3.txz.txt | base64 -d | tar Jxvf -
```

```bash
curl -fsSL https://gitlab.com/hecdelatorre/custom-built-software/-/raw/main/x64/wget2-2.0.1.txz.txt | base64 -d | tar Jxvf -
```
